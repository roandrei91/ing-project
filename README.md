# ING PROJECT

## Prerequisite
* Git client to obtain the sample code
* Docker and docker-compose to start the application
* Postman or curl in order to test the application

## Steps

1. [What is suppose to do](#1-what-is-suppose-to-do)
2. [Get and build the sample code](#2-get-and-build-the-sample-code)
3. [Starting the application](#3-starting-the-application)
4. [Test the application ](#4-test-the-application)

# 1. What is suppose to do

   Write an endpoint that will allow an existing user to open a savings account. The account can 
   only be opened from Monday to Friday within working hours and the user can have only one savings 
   account. The data can be stored however you see fit (e.g. database, in memory, file) 
   and you can use any JVM language and framework.

   Optional: Create a web form that will provide the data to the backend application.

   Add a readme file that will explain how to run the application and any other information
   that you may see relevant.

   Share the application through github or bitbucket or as zip archive.

# 2. Get and build the sample code
    .First, clone the repository.
    
    ```console
    $ git clone https://gitlab.com/roandrei91/ing-project.git   
    $ cd to-cloned-directory
    ```
    
    Then, build the code with Gradle. This will also run tests to make sure everything is working properly. The repository includes the Gradle Wrapper `gradlew` which you can use if you don't have Gradle installed on your system.
    
    ```console
    $ ./gradlew build
    
    BUILD SUCCESSFUL in 1s
    2 actionable tasks: 2 executed
    ```
    
# 3. Starting the application
    To run the applicaton :
    ```console
    docker-compose up --build
     ```
    To stop :
     ```console
     docker-compose down
    ```
    
# 4. Test the application 
   
   Existing user:  User=ingExistingUser / Password=ingPassword 
   
   Accessing http://localhost:8085/account endpoint with basic authentication in order to create
   a new savings account, from authentication I extract the username and check if already an 
   account exists for that specific user, if not and if the day when request is made, is within 
   days of week, between 9:30 AM to 17:00 PM an account should be created and saved in Postgres db 
   otherwise you will get error messages with what you should do further. 
    
   * Add basic authentication with above credentials and access the following endpoint without a body.
           POST - http://localhost:8085/account  
           
   * After an account is successfully done, savings could be added to the account via: 
           POST - http://localhost:8085/account/addMoney
     with following JSON body:
          { 
           "moneyToAdd":30.00
          }
          
   P.S.: In order to create an account time should be within specified working hours in a working 
   day. :) 
  
    
     
    
    
    
