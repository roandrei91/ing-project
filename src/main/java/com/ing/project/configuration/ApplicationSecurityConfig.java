package com.ing.project.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity httpSecurity) throws Exception {
    httpSecurity.csrf().disable()
        .authorizeRequests().antMatchers("/**").hasRole("USER").anyRequest().authenticated()
        .and().httpBasic();

  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder authentication)
      throws Exception
  {
    authentication.inMemoryAuthentication()
        .withUser("ingExistedUser")
        .password(passwordEncoder().encode("ingPassword"))
        .authorities("ROLE_USER");

    authentication.inMemoryAuthentication()
        .withUser("user")
        .password(passwordEncoder().encode("user"))
        .authorities("ROLE_ADMIN");
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }
}

