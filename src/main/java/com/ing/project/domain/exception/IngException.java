package com.ing.project.domain.exception;

public class IngException extends RuntimeException {

  private final ErrorCode errorCode;

 public IngException(ErrorCode errorCode){
   super(errorCode != null ? errorCode.getErrorName() : "");
   this.errorCode = errorCode;
 }

  public ErrorCode getErrorCode() {
    return errorCode;
  }

}