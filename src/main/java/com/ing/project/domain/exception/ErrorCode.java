package com.ing.project.domain.exception;

public enum ErrorCode {


    INVALID_DATE_TIME("Invalid data time", "400", "Account can be created from Monday to Friday, within working hours, 9:30 AM - 17:00 PM."),
    ACCOUNT_ALREADY_EXISTS("Account already exists", "406", ""),
    ACCOUNT_NOT_FOUND("Account not fount", "404", "");

    private final String errorName;
    private final String errorCodeValue;
    private final String errorDetails;

    ErrorCode(String errorName, String errorCodeValue, String errorDetails) {
        this.errorName = errorName;
        this.errorCodeValue = errorCodeValue;
        this.errorDetails = errorDetails;
    }

    public String getErrorName() {
        return errorName;
    }

    public String getErrorCodeValue() {
        return errorCodeValue;
    }

    public String getErrorDetails() {
        return errorDetails;
    }
}
