package com.ing.project.domain.converter;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public interface DataConverter<E, T> {

    T createFrom(E object);

    default List<T> createFromCollection(final Collection<E> objects) {
        return objects.stream()
                .map(this::createFrom)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

}
