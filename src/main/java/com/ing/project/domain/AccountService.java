package com.ing.project.domain;

import com.ing.project.adaptor.rest.dto.AccountResponse;
import com.ing.project.domain.constants.IngConstants;
import com.ing.project.domain.entity.Account;
import com.ing.project.domain.exception.ErrorCode;
import com.ing.project.domain.exception.IngException;
import com.ing.project.domain.model.AccountDto;
import com.ing.project.domain.port.AccountRepositoryPort;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AccountService {

  @Autowired
  private AccountRepositoryPort accountRepositoryPort;

  public void createAccount(String userName) {
    checkIfAccountAlreadyExist(userName);

    final ZonedDateTime dateTimeNow = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("+3"));

    isWithinWorkingHoursAndDays(dateTimeNow);

    final Account account = Account.createInstance(userName, dateTimeNow);

    accountRepositoryPort.save(account);
    log.info("createAccount: Savings account was created with date {} and userName {}", dateTimeNow, userName);
  }

  public AccountResponse addMoney(AccountDto accountDto, String userName) {
    Account account = accountRepositoryPort.findAccountByUserAccountName(userName)
        .orElseThrow(() -> new IngException(ErrorCode.ACCOUNT_NOT_FOUND));
    account.addMoneyToAccount(accountDto.getMoneyToAdd());
    accountRepositoryPort.save(account);

    log.info("addMoney: Money added for userName {}, new account money balance {}.", userName, account.getSavingsMoneyBalance());

    return AccountResponse.builder()
              .message(IngConstants.ADD_MONEY_RESPONSE_MESSAGE)
              .savingsAccountBalance(account.getSavingsMoneyBalance())
              .build();
  }

  private void checkIfAccountAlreadyExist(String userAccountName) {
    Optional<Account> account = accountRepositoryPort.findAccountByUserAccountName(userAccountName);
    if (account.isPresent()) {
      throw new IngException(ErrorCode.ACCOUNT_ALREADY_EXISTS);
    }
  }

  private boolean isWithinWorkingHoursAndDays(ZonedDateTime zonedDateTime) {
    Calendar d = GregorianCalendar.from(zonedDateTime);
    Calendar begin = Calendar.getInstance();
    Calendar end = Calendar.getInstance();

    begin.set(Calendar.HOUR, 9);
    begin.set(Calendar.MINUTE, 29);
    begin.set(Calendar.AM_PM, Calendar.AM);

    end.set(Calendar.HOUR, 5);
    end.set(Calendar.MINUTE, 01);
    end.set(Calendar.AM_PM, Calendar.PM);

    if (d.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY && d.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
        && (d.after(begin) && d.before(end))) {
      return true;
    }
    throw new IngException(ErrorCode.INVALID_DATE_TIME);
  }
}
