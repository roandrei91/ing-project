package com.ing.project.domain.entity;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Account {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(unique = true)
  private String userAccountName;

  private double savingsMoneyBalance;

  private ZonedDateTime createdDate;

  public void addMoneyToAccount(double money) {
    savingsMoneyBalance = savingsMoneyBalance + money;
  }

  public static Account createInstance(String userAccountName, ZonedDateTime createdDate) {
    final Account account = new Account();
    account.setUserAccountName(userAccountName);
    account.setCreatedDate(createdDate);
    return account;
  }

}
