package com.ing.project.domain.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AccountDto {

  private double moneyToAdd;

}
