package com.ing.project.domain.port;

import com.ing.project.domain.entity.Account;
import java.util.Optional;

public interface AccountRepositoryPort {

  void save(Account account);

  Optional<Account> findAccountByUserAccountName(String userAccountName);

}
