package com.ing.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IngProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(IngProjectApplication.class, args);
	}

}
