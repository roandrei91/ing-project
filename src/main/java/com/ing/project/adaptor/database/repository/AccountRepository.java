package com.ing.project.adaptor.database.repository;

import com.ing.project.domain.entity.Account;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

  Optional<Account> findAccountByUserAccountName(String userAccountName);
}
