package com.ing.project.adaptor.database.operations;

import com.ing.project.adaptor.database.repository.AccountRepository;
import com.ing.project.domain.entity.Account;
import com.ing.project.domain.port.AccountRepositoryPort;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountOperation implements AccountRepositoryPort {

  @Autowired
  private AccountRepository accountRepository;

  @Override
  public void save(Account account) {
     accountRepository.save(account);
  }

  @Override
  public Optional<Account> findAccountByUserAccountName(String userAccountName) {
    return accountRepository.findAccountByUserAccountName(userAccountName);
  }

}
