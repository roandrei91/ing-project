package com.ing.project.adaptor.rest;

import com.ing.project.adaptor.rest.dto.AccountResponse;
import com.ing.project.domain.AccountService;
import com.ing.project.domain.model.AccountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
public class IngController {

  @Autowired
  private AccountService accountService;

  @PostMapping("/addMoney")
  public ResponseEntity<AccountResponse> addMoney(@RequestBody AccountDto accountDto, Authentication authentication) {
    final AccountResponse response = accountService.addMoney(accountDto, authentication.getName());
    return ResponseEntity.accepted().body(response);
  }

  @PostMapping
  public ResponseEntity<String> add(Authentication authentication) {
     accountService.createAccount(authentication.getName());
    return ResponseEntity.accepted().build();
  }

}
