package com.ing.project.adaptor.rest.dto;

import lombok.Getter;

@Getter
public class ErrorDto {

  private final String status;

  public ErrorDto(String status) {
    this.status = status;
  }
}
