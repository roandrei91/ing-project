package com.ing.project.adaptor.rest;

import com.ing.project.adaptor.rest.dto.ErrorDto;
import com.ing.project.domain.exception.ErrorCode;
import com.ing.project.domain.exception.IngException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlingController {

  @ExceptionHandler({IngException.class})
  @ResponseBody
  public ResponseEntity<ErrorDto> handleValidatorException(IngException e) {
    if (ErrorCode.ACCOUNT_ALREADY_EXISTS.name().equals(e.getErrorCode().name())) {
      return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new ErrorDto(e.getErrorCode().getErrorName()));
    }
    if (ErrorCode.ACCOUNT_NOT_FOUND.name().equals(e.getErrorCode().name())) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorDto(e.getErrorCode().getErrorName()));
    }
    if (ErrorCode.INVALID_DATE_TIME.name().equals(e.getErrorCode().name())) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorDto(e.getErrorCode().getErrorDetails()));
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorDto("Internal server error."));
  }
}
