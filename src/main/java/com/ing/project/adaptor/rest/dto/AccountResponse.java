package com.ing.project.adaptor.rest.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccountResponse {

  private String message;
  private double savingsAccountBalance;

}
